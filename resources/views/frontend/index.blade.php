@extends('frontend.layout.main')

@section('pagespecificstyles')
    <link rel="stylesheet" href="{{asset('asset/frontend/css/home_style.css')}}">

    <!--pagespecificstyles -->
@stop

@section('content')
    <div class="main_content">
        <!-- banner part start-->
    @include("frontend.elements.include.index_page.banner")
    <!-- banner part start-->

        <!-- feature_part start-->
    @include("frontend.elements.include.index_page.feature_part")
    <!-- feature_part end-->

        <!-- upcoming_event part start-->
        <!-- upcoming_event part end-->

        <!-- new arrival part here -->
    @include("frontend.elements.include.index_page.new_arrival")
    <!-- new arrival part end -->

    </div>
@endsection
@section('pagespecificscripts')
    <!--pagespecificscripts -->
@stop
