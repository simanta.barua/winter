@extends('frontend.layout.main')

@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">
@stop
@section('content')
    <div class="main_content">
        <!-- breadcrumb start-->
    <!-- breadcrumb start-->

        <!--================Category Product Area =================-->
    @include("frontend.elements.include.category_page.category_part")
    <!--================End Category Product Area =================-->

    </div>
@endsection

@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/price_rangs.js')}}"></script>

@stop
