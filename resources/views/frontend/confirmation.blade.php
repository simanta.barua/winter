
@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">
@stop
@section('content')
    <div class="main_content">
        <!--================ confirmation part start =================-->
        <section class="confirmation_part section_padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="confirmation_tittle">
                            <span>Thank you. Your order has been received.</span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lx-4">
                        <div class="single_confirmation_details">
                            <h4>order info</h4>
                            <ul>
                                <li>
                                    <p>order number</p><span>: 60235</span>
                                </li>
                                <li>
                                    <p>data</p><span>: Oct 03, 2017</span>
                                </li>
                                <li>
                                    <p>total</p><span>: USD 2210</span>
                                </li>
                                <li>
                                    <p>mayment methord</p><span>: Check payments</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lx-4">
                        <div class="single_confirmation_details">
                            <h4>Billing Address</h4>
                            <ul>
                                <li>
                                    <p>Street</p><span>: 56/8</span>
                                </li>
                                <li>
                                    <p>city</p><span>: Los Angeles</span>
                                </li>
                                <li>
                                    <p>country</p><span>: United States</span>
                                </li>
                                <li>
                                    <p>postcode</p><span>: 36952</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lx-4">
                        <div class="single_confirmation_details">
                            <h4>shipping Address</h4>
                            <ul>
                                <li>
                                    <p>Street</p><span>: 56/8</span>
                                </li>
                                <li>
                                    <p>city</p><span>: Los Angeles</span>
                                </li>
                                <li>
                                    <p>country</p><span>: United States</span>
                                </li>
                                <li>
                                    <p>postcode</p><span>: 36952</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="order_details_iner">
                            <h3>Order Details</h3>
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2">Product</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                                    <th>x02</th>
                                    <th> <span>$720.00</span></th>
                                </tr>
                                <tr>
                                    <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                                    <th>x02</th>
                                    <th> <span>$720.00</span></th>
                                </tr>
                                <tr>
                                    <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                                    <th>x02</th>
                                    <th> <span>$720.00</span></th>
                                </tr>
                                <tr>
                                    <th colspan="3">Subtotal</th>
                                    <th> <span>$2160.00</span></th>
                                </tr>
                                <tr>
                                    <th colspan="3">shipping</th>
                                    <th><span>flat rate: $50.00</span></th>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th scope="col" colspan="3">Quantity</th>
                                    <th scope="col">Total</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================ confirmation part end =================-->
    </div>
@endsection


@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
    <script src="{{asset('asset/frontend/js/stellar.js')}}"></script>
    <script src="{{asset('asset/frontend/js/price_rangs.js')}}"></script>
@stop
