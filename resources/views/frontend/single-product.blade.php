@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/lightslider.min.css')}}">
@stop
@section('content')
    <div class="main_content">

        <!--================Single Product Area =================-->
    @include("frontend.elements.include.single-product_page.single_product_area")
    <!--================End Single Product Area =================-->

        <!--================Product Description Area =================-->
    @include("frontend.elements.include.single-product_page.product_description_area")
    <!--================End Product Description Area =================-->

        <!-- product_list part start-->
    @include("frontend.elements.include.single-product_page.product_list_part_area")
    <!-- product_list part end-->
    </div>
@endsection

@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <!-- swiper js -->
    <script src="{{asset('asset/frontend/js/lightslider.min.js')}}"></script>
    <!-- particles js -->
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <!-- slick js -->
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
@stop

