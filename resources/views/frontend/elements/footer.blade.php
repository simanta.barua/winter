<!-- free shipping here -->
@include("frontend.elements.include.footer_page.shipping_details")
<!-- free shipping end -->

<!--::footer_part start::-->
@include("frontend.elements.include.footer_page.news_latter")
<!--::footer_part end::-->
<!--::footer_part start::-->
@include("frontend.elements.include.footer_page.footer_part")
<!--::footer_part end::-->

<!-- jquery plugins here-->
<script src="{{asset('asset/frontend/js/jquery-1.12.1.min.js')}}"></script>
<!-- popper js -->
<script src="{{asset('asset/frontend/js/popper.min.js')}}"></script>

<!-- bootstrap js -->
<script src="{{asset('asset/frontend/js/bootstrap.min.js')}}"></script>
<!-- easing js -->
<script src="{{asset('asset/frontend/js/jquery.magnific-popup.js')}}"></script>
<!-- swiper js -->
<script src="{{asset('asset/frontend/js/swiper.min.js')}}"></script>
<!-- swiper js -->
<script src="{{asset('asset/frontend/js/mixitup.min.js')}}"></script>

<!-- particles js -->
<script src="{{asset('asset/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
<!-- slick js -->
<script src="{{asset('asset/frontend/js/slick.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/waypoints.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/mail-script.js')}}"></script>
@yield('pagespecificscripts')

<!-- custom js -->
<script src="{{asset('asset/frontend/js/custom.js')}}"></script>
<!-- page specific scripts -->
