<link rel="icon" href="{{asset('asset/frontend/img/favicon.png')}}">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/bootstrap.min.css')}}">
<!-- animate CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/animate.css')}}">
<!-- owl carousel CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/owl.carousel.min.css')}}">
<!-- font awesome CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/all.css')}}">
<!-- flaticon CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('asset/frontend/css/themify-icons.css')}}">
<!-- font awesome CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/magnific-popup.css')}}">
<!-- swiper CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/slick.css')}}">
<!-- style CSS -->
<link rel="stylesheet" href="{{asset('asset/frontend/css/style.css')}}">

<!--pagespecificstyles-->
@yield('pagespecificstyles')

