<!-- product_list part start-->
<section class="product_list best_seller padding_bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="section_tittle text-center">
                    <h2>Best Sellers</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single_category_product">
                    <div class="single_category_img">
                        <img src="{{asset('asset/frontend/img/category/category_2.png')}}" alt="">
                        <div class="category_social_icon">
                            <ul>
                                <li><a href="#"><i class="ti-heart"></i></a></li>
                                <li><a href="#"><i class="ti-bag"></i></a></li>
                            </ul>
                        </div>
                        <div class="category_product_text">
                            <a href="single-product.blade.php"><h5>Long Sleeve TShirt</h5></a>
                            <p>$150.00</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_category_product">
                    <div class="single_category_img">
                        <img src="{{asset('asset/frontend/img/category/category_3.png')}}" alt="">
                        <div class="category_social_icon">
                            <ul>
                                <li><a href="#"><i class="ti-heart"></i></a></li>
                                <li><a href="#"><i class="ti-bag"></i></a></li>
                            </ul>
                        </div>
                        <div class="category_product_text">
                            <a href="single-product.blade.php"><h5>Long Sleeve TShirt</h5></a>
                            <p>$150.00</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_category_product">
                    <div class="single_category_img">
                        <img src="{{asset('asset/frontend/img/category/category_4.png')}}" alt="">
                        <div class="category_social_icon">
                            <ul>
                                <li><a href="#"><i class="ti-heart"></i></a></li>
                                <li><a href="#"><i class="ti-bag"></i></a></li>
                            </ul>
                        </div>
                        <div class="category_product_text">
                            <a href="single-product.blade.php"><h5>Long Sleeve TShirt</h5></a>
                            <p>$150.00</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_category_product">
                    <div class="single_category_img">
                        <img src="{{asset('asset/frontend/img/category/category_5.png')}}" alt="">
                        <div class="category_social_icon">
                            <ul>
                                <li><a href="#"><i class="ti-heart"></i></a></li>
                                <li><a href="#"><i class="ti-bag"></i></a></li>
                            </ul>
                        </div>
                        <div class="category_product_text">
                            <a href="single-product.blade.php"><h5>Long Sleeve TShirt</h5></a>
                            <p>$150.00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- product_list part end-->

