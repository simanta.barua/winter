@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">
@stop
@section('page_breadcrumb')
    about
    @stop
@section('content')
    <div class="main_content">

        <!-- about part here -->
    @include("frontend.elements.include.about_page.about_part")
    <!-- about part end -->


    </div>
@endsection

@section('about_js')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
    <script src="{{asset('asset/frontend/js/price_rangs.js')}}"></script>

@stop
