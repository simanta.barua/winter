@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">
@stop
@section('content')
    <div class="main_content">
        <!--================Tracking Box Area =================-->
@include("frontend.elements.include.tracking_page.tracking_box_area")
        <!--================End Tracking Box Area =================-->
    </div>
@endsection


@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/stellar.js')}}"></script>
    <script src="{{asset('asset/frontend/js/price_rangs.js')}}"></script>
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
@stop


