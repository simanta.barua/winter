    @extends('frontend.layout.main')
    @section('pagespecificstyles')
        <!--pagespecificstyles -->
        <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
        <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">

    @stop
    @section('content')
        <div class="main_content">
            <!--================Blog Area =================-->
            @include("frontend.elements.include.blog_page.blog_area")
            <!--================Blog Area =================-->
        </div>
    @endsection


    @section('pagespecificscripts')
        <!--pagespecificscripts -->
         <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
    @stop
