@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
@stop
@section('content')
    <div class="main_content">
        <!--================Checkout Area =================-->
    @include("frontend.elements.include.checkout_page.checkout_area")
    <!--================End Checkout Area =================-->
    </div>
@endsection

@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
@stop
