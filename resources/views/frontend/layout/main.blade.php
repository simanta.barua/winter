<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>winter</title>
    @include('frontend.elements.include.header.head')
</head>
<body>
@include("frontend.elements.header")
<!-- main content -->
<div id="main_wrapper">
    <div class="page_content">
        @yield('content')
    </div>
</div>
@include("frontend.elements.footer")

</body>
</html>
