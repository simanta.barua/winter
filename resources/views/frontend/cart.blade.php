@extends('frontend.layout.main')
@section('pagespecificstyles')
    <!--pagespecificstyles -->
    <link rel="icon" href="{{asset('asset/frontend/img/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('asset/frontend/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('asset/frontend/css/price_rangs.css')}}">
@stop @section('content')
    <div class="main_content">
        <!--================Cart Area =================-->
    @include("frontend.elements.include.cart_page.cart_area")
    <!--================End Cart Area =================-->
    </div>

@endsection

@section('pagespecificscripts')
    <!--pagespecificscripts -->
    <script src="{{asset('asset/frontend/js/price_rangs.js')}}"></script>
    <script src="{{asset('asset/frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('asset/frontend/js/contact.js')}}"></script>
@stop
