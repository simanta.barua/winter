<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#"><img src="{{asset('asset/backend/images/logo.png')}}" alt="Logo"></a>
            <a class="navbar-brand hidden" href="#"><img src="{{asset('asset/backend/images/logo2.png')}}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{asset('asset/backend/index.html')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="{{route('brands.index')}}" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-product-hunt">
                        </i>Brands</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-eye "></i><a href="{{route('brands.index')}}">All Brands</a></li>
                        <li><i class="fa fa-edit "></i><a href="{{route('brands.create')}}">Create Brands</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="{{route('category.index')}}" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-product-hunt"></i>Categories</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-eye"></i><a href="{{route('category.index')}}">All Category</a></li>
                        <li><i class="fa fa-edit"></i><a href="{{route('category.create')}}">Create Category</a></li>
                    </ul>
                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->
