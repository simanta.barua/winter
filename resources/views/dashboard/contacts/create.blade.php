@extends('dashboard.layout.backendlayout_main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"> <strong> Contact Name </strong> </div>
            <div class="card-body card-block">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success"> {{session('message')}}  </div>
                @endif
                {!! Form::open(array('url' => 'contacts','method' => 'POST')) !!}

                <div class="form-group">
                    {{Form::label('contactName', "Contact Name")}}
                    {{Form::text('contactName', null, ['class'=>'form-control','id' => 'contactName','required', 'placeholder'=>'Contact Name'])}}
                </div>
                <div class="from-group">
                    {{Form::label('email',"Email")}}
                    {{Form::text('email',null,['class'=>'form-control','id'=>'email','required','placeholder'=>'Email'])}}
                </div>
                <div class="from-group">
                    {{Form::label('subject',"Subject")}}
                    {{Form::text('subject',null,['class'=>'form-control','id'=>'subject','required','placeholder'=>'Subject'])}}
                </div>
                <div class="from-group">
                    {{Form::label('comment',"Comment")}}
                    {{Form::text('comment',null,['class'=>'form-control','id'=>'comment','required','placeholder'=>'Comment'])}}
                </div>

                {{Form::submit('Add Contact', ['class'=>'btn btn-success'])}}
                {!! Form::close() !!}






            </div> <!-- end card-body card-block -->
        </div> <!-- card -->
    </div> <!-- end col-md-12 -->
@endsection('content')
