@extends('dashboard.layout.backendlayout_main')
@section('content')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                <th>Title</th>
                                <th>Link</th>
                                <th> Draft </th>
                                <th> Active </th>
                                <th> Soft Delete </th>
                                <th> Actions </th>
                                </tr>
                                </thead>
                            <tbody>
                            @foreach($brands as $brand)
                                <tr>
                                    <td></td>
                                    <td>{{$brand->title}}</td>
                                    <td>{{$brand->link}}</td>
                                    <td>{{$brand->is_draft}}</td>
                                    <td>{{$brand->is_active}}</td>
                                    <td>{{$brand->soft_delete}}</td>
                                    <td>
                                        <a href="{{route('brands.show',$brand->id)}}" class="btn btn-success "> <i class="fa fa-eye"></i></a>
                                        <a href="{{route('brands.edit',$brand->id)}}" class="btn btn-warning "><i class="fa fa-edit"></i> </a>
                                        <a href="#" class="btn btn-danger"> <i class="fa fa-trash-o "></i> </a>
                                    </td>
                                </tr>
                            </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection()
