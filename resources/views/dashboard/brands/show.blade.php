@extends('dashboard.layout.backendlayout_main')
@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="float-left">Brand Data</h4>
                <a href="{{route('brands.index')}}" class="btn btn-primary float-right">Brand List</a>
            </div>
            <div class="card-body py-5 " style="background: #E9ECEF;">
                <table class="table m-auto float-left" style="max-width: 700px;">
                    <tbody>
                    <tr>
                        <th scope="col">Title:</th>
                        <td scope="col">{{$brand->title}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Link:</th>
                        <td scope="col">{{$brand->link}}</td>
                    <tr>
                        <th scope="col">Activate Banner:</th>
                        <td scope="col">{{$brand->is_active}}</td>
                    </tr>

                    </tr>
                    <tr>
                        <th scope="col">Draft Banner:</th>
                        <td scope="col">{{$brand->is_draft}}</td>
                    </tr>
                    </tbody>
                    </tr>
                    <tr>
                        <th scope="col">Created Time:</th>
                        <td scope="col">{{$brand->created_at}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Modified Time:</th>
                        <td scope="col">{{$brand->updated_at}}</td>
                    </tr>
                    <tr>
                </table>
            </div>

        </div>
@endsection
