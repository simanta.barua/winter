@extends('dashboard.layout.backendlayout_main')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"> <strong> Brand Name </strong> </div>
            <div class="card-body card-block">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success"> {{session('message')}}  </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(array('url' => 'brands','method' => 'POST')) !!}

                        <div class="form-group">
                            {{Form::label('title', 'Title')}}
                            {{Form::text('title', old('title') ? old('title') : (!empty($brand)? $brand->title : null),
                                [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter Title Here..',
                                ]
                            )}}
                        </div>

                        <div class="form-group">
                            {{Form::label('link', 'Link')}}
                            {{Form::text('link', old('link') ? old('link') : (!empty($brand)? $brand->link : null),
                                [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter Link Here..',
                                ]
                            )}}
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Save as Draft </label>
                            <div class="col-sm-4">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{Form::radio('is_draft', true, false, ['class' =>'form-check-input'])}}
                                        Yes
                                        <i class="input-helper"></i></label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{Form::radio('is_draft', 0, true, ['class' =>'form-check-input'])}}
                                        No
                                        <i class="input-helper"></i></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Active </label>
                            <div class="col-sm-4">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{Form::radio('is_active', true, true, ['class' =>'form-check-input'])}}
                                        Yes
                                        <i class="input-helper"></i></label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{Form::radio('is_active', 0, false, ['class' =>'form-check-input'])}}
                                        No
                                        <i class="input-helper"></i></label>
                                </div>
                            </div>
                        </div>



                        {{Form::submit('Add Brand', ['class'=>'btn btn-success'])}}
                        <a href="{{route('brands.index')}}" class="btn btn-warning"> Cancel </a>

                        {!! Form::close() !!}




                    </div>

                </div> <!-- end card-body card-block -->
            </div> <!-- card -->
        </div> <!-- end col-md-12 -->
@endsection('content')
