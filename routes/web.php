<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Route::get('/about', function () {
    return view('frontend.about');
});
Route::get('/blog', function () {
    return view('frontend.blog');
});
Route::get('/cart', function () {
    return view('frontend.cart');
});
Route::get('/category', function () {
    return view('frontend.category');
});
Route::get('/checkout', function () {
    return view('frontend.checkout');
});
Route::get('/confirmation', function () {
    return view('frontend.confirmation');
});
Route::get('/contact', function () {
    return view('frontend.contact');
});
Route::get('/elements', function () {
    return view('frontend.elements');
});
Route::get('/login', function () {
    return view('frontend.login');
});
Route::get('/single-blog', function () {
    return view('frontend.single-blog');
});
Route::get('/single-product', function () {
    return view('frontend.single-product');
});
Route::get('/tracking', function () {
    return view('frontend.tracking');
});
Route::get('/dashboard', function () {
    return view('dashboard.index');
});
Route::resource('brands', 'BrandController');
Route::resource('contacts', 'ContactController');
Route::resource('category','CategoryController');
